/* -*-c++-*-
    dopt - C++ command line option parser.

    This is free and unencumbered software released into the public domain.
*/

#ifndef DOPT_VALUE_HPP
#define DOPT_VALUE_HPP

#include <optional>
#include <sstream>
#include <string>

namespace dopt
{

/**
 * Stores an argument value.
 */
class Value
{
public:
    /**
     * Default constructor.
     */
    Value() = default;

    /**
     * Copy constructor.
     */
    Value(const Value &) = default;

    /**
     * Move constructor.
     */
    Value(Value &&) = default;

    /**
     * Copy assignment operator.
     */
    Value &operator=(const Value &) = default;

    /**
     * Move assignment operator.
     */
    Value &operator=(Value &&) = default;

    /**
     * Constructor.
     * 
     * Parameters:
     *   arg - A string.
     */
    Value(const std::string &arg)
        : _arg(arg) {}

    /**
     * Constructor.
     * 
     * Parameters:
     *   arg - A C-string.
     */
    Value(const char *arg)
        : _arg(arg) {}

    /**
     * Check whether the object holds a value.
     */
    bool has_value() const
    {
        return _arg.has_value();
    }

    /**
     * Return the value as a string.
     */
    const std::string &str() const
    {
        return _arg.value();
    }

    /**
     * Converts the value to given type T.
     * 
     * Parameters:
     *   default_value - A default value returned if the Value object is empty.
     */
    template <typename T>
    auto get(T default_value = T()) const
    {
        if (_arg.has_value())
        {
            std::istringstream is(_arg.value());
            is >> default_value;
        }
        return default_value;
    }

    /**
     * Check whether the object holds a value.
     */
    operator bool() const
    {
        return has_value();
    }

private:
    std::optional<std::string> _arg;
};

} // namespace dopt

#endif