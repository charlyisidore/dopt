/* -*-c++-*-
    dopt - C++ command line option parser.

    This is free and unencumbered software released into the public domain.
*/

#ifndef DOPT_ARGUMENT_HPP
#define DOPT_ARGUMENT_HPP

#include <string>

namespace dopt
{

/**
 * Defines an option argument.
 */
class Argument
{
public:
    /**
     * Constructor.
     * 
     * Parameters:
     *   name - The argument name (e.g. "FILE").
     */
    Argument(const std::string &name)
        : _name(name), _required(true) {}

    /**
     * Copy constructor.
     */
    Argument(const Argument &) = default;

    /**
     * Move constructor.
     */
    Argument(Argument &&) = default;

    /**
     * Copy assignment operator.
     */
    Argument &operator=(const Argument &) = default;

    /**
     * Move assignment operator.
     */
    Argument &operator=(Argument &&) = default;

    /**
     * Mark the argument as required.
     */
    Argument &required()
    {
        _required = true;
        return *this;
    }

    /**
     * Mark the argument as optional.
     */
    Argument &optional()
    {
        _required = false;
        return *this;
    }

    /**
     * Return the name of the argument.
     */
    const std::string &name() const
    {
        return _name;
    }

    /**
     * Check whether the argument is required.
     */
    bool is_required() const
    {
        return _required;
    }

    /**
     * Check whether the argument is optional.
     */
    bool is_optional() const
    {
        return !_required;
    }

private:
    std::string _name;
    bool _required = true;
};

} // namespace dopt

#endif