/* -*-c++-*-
    dopt - C++ command line option parser.

    This is free and unencumbered software released into the public domain.
*/

#ifndef DOPT_MAP_HPP
#define DOPT_MAP_HPP

#include "name.hpp"
#include "option.hpp"
#include "value.hpp"
#include <map>
#include <string>
#include <vector>

namespace dopt
{

/**
 * Stores parsing results.
 */
class Map
{
public:
    /**
     * Default constructor.
     */
    Map() = default;

    /**
     * Copy constructor.
     */
    Map(const Map &) = default;

    /**
     * Move constructor.
     */
    Map(Map &&) = default;

    /**
     * Copy assignment constructor.
     */
    Map &operator=(const Map &) = default;

    /**
     * Move assignment constructor.
     */
    Map &operator=(Map &&) = default;

    /**
     * Return the number of positional arguments.
     */
    std::size_t size() const
    {
        return _positional.size();
    }

    /**
     * Access the value of a short option.
     */
    Value operator[](char key) const
    {
        auto i = _indices.find(key);
        return i != _indices.end() ? _named.at(i->second) : Value();
    }

    /**
     * Access the value of a long option.
     */
    Value operator[](const std::string &name) const
    {
        auto i = _indices.find(name);
        return i != _indices.end() ? _named.at(i->second) : Value();
    }

    /**
     * Access the value of a positional argument.
     */
    Value operator[](int index) const
    {
        return _positional.at(index);
    }

    /**
     * Set the value of a named option.
     */
    void assign(const std::vector<Name> &names, const Value &value)
    {
        auto it = _indices.find(names.front());
        if (it == _indices.end())
        {
            for (const auto &name : names)
            {
                _indices.insert({name, _named.size()});
            }
            _named.push_back(value);
        }
        else
        {
            _named.at(it->second) = value;
        }
    }

    /**
     * Add a positional argument.
     * 
     * Parameters:
     *   value - A value.
     */
    void push_back(const Value &value)
    {
        _positional.push_back(value);
    }

private:
    std::map<Name, std::size_t> _indices;
    std::vector<Value> _named;
    std::vector<Value> _positional;
};

} // namespace dopt

#endif