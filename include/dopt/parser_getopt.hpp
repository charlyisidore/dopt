/* -*-c++-*-
    dopt - C++ command line option parser.

    This is free and unencumbered software released into the public domain.
*/

#ifndef DOPT_PARSER_GETOPT_HPP
#define DOPT_PARSER_GETOPT_HPP

#include "map.hpp"
#include "option.hpp"
#include <getopt.h>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

namespace dopt
{

/**
 * Parse command line options using getopt.
 */
class ParserGetopt
{
public:
    /**
     * Constructor.
     * 
     * Parameters:
     *   options - A list of options.
     */
    ParserGetopt(const std::vector<Option> &options)
        : _options(options)
    {
        _build();
    }

    /**
     * Deleted copy constructor.
     */
    ParserGetopt(const ParserGetopt &) = delete;

    /**
     * Move constructor.
     */
    ParserGetopt(ParserGetopt &&) = default;

    /**
     * Deleted copy assignment operator.
     */
    ParserGetopt &operator=(const ParserGetopt &) = delete;

    /**
     * Move assignment operator.
     */
    ParserGetopt &operator=(ParserGetopt &&) = default;

    /**
     * Parse a given command line.
     */
    Map operator()(int argc, char *argv[]) const
    {
        return _parse(argc, argv);
    }

private:
    /**
     * Build an array of getopt options.
     */
    void _build()
    {
        int index = 0x100;

        for (const auto &option : _options)
        {
            const auto &names = option.names();
            for (auto n = std::begin(names); n != std::end(names); ++n)
            {
                if (n->is_short())
                {
                    char key = n->get_short();
                    _shortopts += key;
                    if (option.has_argument())
                    {
                        if (option.argument().is_required())
                        {
                            _shortopts += ":";
                        }
                        else // is_optional
                        {
                            _shortopts += "::";
                        }
                    }
                    _getopt_keys.insert({key, option});
                }
                else // is_long
                {
                    struct ::option longopt = {};
                    longopt.name = n->get_long().c_str();
                    if (option.has_argument())
                    {
                        if (option.argument().is_required())
                        {
                            longopt.has_arg = required_argument;
                        }
                        else // is_optional
                        {
                            longopt.has_arg = optional_argument;
                        }
                    }
                    else
                    {
                        longopt.has_arg = no_argument;
                    }
                    longopt.val = index++;
                    _getopt_keys.insert({longopt.val, option});
                    _longopts.push_back(longopt);
                }
            }
        }
        _longopts.push_back({});
    }

    /**
     * Parse a given command line.
     */
    Map _parse(int argc, char *argv[]) const
    {
        Map map;
        int index = 0;

        for (int key = 0; key != -1;)
        {
            key = getopt_long(argc,
                              argv,
                              _shortopts.c_str(),
                              _longopts.data(),
                              &index);

            switch (key)
            {
                case -1: // End of options
                case 0:  // Option setting a flag
                    break;

                case '?':
                    // getopt already has printed an error message
                    // optopt != 0 => requires an argument
                    // optopt == 0 => unrecognized option
                    std::exit(1);

                default:
                    auto it = _getopt_keys.find(key);
                    if (it == _getopt_keys.end())
                    {
                        std::cerr << "unknown option character `\\x" << std::hex << key << "'." << std::endl;
                        std::exit(1);
                    }
                    map.assign(it->second.names(), optarg ? optarg : std::string());
                    break;
            }
        }

        // Positional arguments
        while (optind < argc)
        {
            map.push_back({argv[optind++]});
        }
        return map;
    }

    std::vector<Option> _options;
    std::unordered_map<int, const Option &> _getopt_keys;
    std::string _shortopts;
    std::vector<struct ::option> _longopts;
};

} // namespace dopt

#endif