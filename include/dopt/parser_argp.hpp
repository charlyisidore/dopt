/* -*-c++-*-
    dopt - C++ command line option parser.

    This is free and unencumbered software released into the public domain.
*/

#ifndef DOPT_PARSER_ARGP_HPP
#define DOPT_PARSER_ARGP_HPP

#include "map.hpp"
#include "option.hpp"
#include <argp.h>
#include <set>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

namespace dopt
{

/**
 * Parse command line options using Argp.
 */
class ParserArgp
{
public:
    /**
     * Constructor.
     * 
     * Parameters:
     *   args_doc - A string describing non-option arguments.
     *   doc - A descripting string about the program displayed before the options.
     *   options - A list of options.
     *   footer - An optional string to be displayed after the options.
     */
    ParserArgp(const std::string &args_doc,
               const std::string &doc,
               const std::vector<Option> &options,
               const std::string &footer = std::string())
        : _args_doc(args_doc), _doc(doc), _options(options)
    {
        if (!footer.empty())
        {
            _doc += "\v" + footer;
        }
        _build();
    }

    /**
     * Deleted copy constructor.
     */
    ParserArgp(const ParserArgp &) = delete;

    /**
     * Move constructor.
     */
    ParserArgp(ParserArgp &&) = default;

    /**
     * Deleted copy assignment operator.
     */
    ParserArgp &operator=(const ParserArgp &) = delete;

    /**
     * Move assignment operator.
     */
    ParserArgp &operator=(ParserArgp &&) = default;

    /**
     * Parse a given command line.
     */
    Map operator()(int argc,
                   char *argv[],
                   const std::set<int> &flags = std::set<int>()) const
    {
        return _parse(argc, argv, flags);
    }

private:
    typedef std::tuple<const ParserArgp &, Map &> _ArgpInput;

    /**
     * Build an array of argp_option.
     */
    void _build()
    {
        int index = 0x100;

        for (const auto &option : _options)
        {
            struct ::argp_option opt = {};

            if (option.has_argument())
            {
                const auto &arg = option.argument();
                opt.arg = arg.name().c_str();
                if (arg.is_optional())
                {
                    opt.flags |= OPTION_ARG_OPTIONAL;
                }
            }

            for (auto flag : option.flags())
            {
                opt.flags |= flag;
            }

            const auto &names = option.names();
            if (!names.empty())
            {
                // Named option
                for (auto n = std::begin(names); n != std::end(names); ++n)
                {
                    if (n->is_short())
                    {
                        opt.key = n->get_short();
                        opt.name = 0;
                    }
                    else
                    {
                        opt.key = index++;
                        opt.name = n->get_long().c_str();
                    }

                    if (n == std::begin(names))
                    {
                        opt.doc = option.doc().c_str();
                    }
                    else
                    {
                        opt.flags |= OPTION_ALIAS;
                        opt.arg = 0;
                        opt.doc = 0;
                    }

                    _argp_keys.insert({opt.key, option});
                    _argp_opts.push_back(opt);
                }
            }
            else
            {
                // Option group title
                opt.doc = option.doc().c_str();
                _argp_opts.push_back(opt);
            }
        }
        _argp_opts.push_back({});
    }

    /**
     * Parse a given command line.
     */
    Map _parse(int argc,
               char *argv[],
               const std::set<int> &flags = std::set<int>()) const
    {
        Map map;
        _ArgpInput input(*this, map);
        int argp_flags = 0;

        struct ::argp argp = {
            _argp_opts.data(),
            _parse_opt,
            _args_doc.c_str(),
            _doc.c_str(),
            nullptr,
            nullptr,
            nullptr};

        for (auto flag : flags)
        {
            argp_flags |= flag;
        }

        argp_parse(&argp, argc, argv, argp_flags, 0, &input);
        return map;
    }

    /**
     * A function called by Argp to parse a single option.
     * 
     * Parameters:
     *   key - Identifier of the option.
     *   arg - Argument of the option, or nullptr if it has none.
     *   state - A pointer to a struct argp_state.
     */
    static error_t _parse_opt(int key, char *arg, struct ::argp_state *state)
    {
        auto [parser, map] = *static_cast<_ArgpInput *>(state->input);

        switch (key)
        {
            case ARGP_KEY_INIT:    // Before parsing is done
            case ARGP_KEY_NO_ARGS: // No positional argument found
            case ARGP_KEY_END:     // No more arguments
            case ARGP_KEY_SUCCESS: // Parsing has been completed
            case ARGP_KEY_ERROR:   // An error has occurred
            case ARGP_KEY_FINI:    // Called after succes/error
            case ARGP_KEY_ARGS:    //
                break;

            case ARGP_KEY_ARG:
                // A positional argument has been found
                map.push_back({arg});
                break;

            default:
                // Named argument or Argp special key
                auto it = parser._argp_keys.find(key);
                if (it == parser._argp_keys.end())
                {
                    // Argp special key?
                    return ARGP_ERR_UNKNOWN;
                }
                map.assign(it->second.names(), arg ? arg : std::string());
                return 0;
        }

        return 0;
    }

    std::string _args_doc;
    std::string _doc;
    std::vector<Option> _options;
    std::unordered_map<int, const Option &> _argp_keys;
    std::vector<struct ::argp_option> _argp_opts;
};

} // namespace dopt

#endif