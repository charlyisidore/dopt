/* -*-c++-*-
    dopt - C++ command line option parser.

    This is free and unencumbered software released into the public domain.
*/

#ifndef DOPT_OPTION_HPP
#define DOPT_OPTION_HPP

#include "argument.hpp"
#include "name.hpp"
#include <optional>
#include <set>
#include <vector>

namespace dopt
{

/**
 * Describes a program option.
 */
class Option
{
public:
    /**
     * Copy constructor.
     */
    Option(const Option &) = default;

    /**
     * Move constructor.
     */
    Option(Option &&) = default;

    /**
     * Copy assignment constructor.
     */
    Option &operator=(const Option &) = default;

    /**
     * Move assignment constructor.
     */
    Option &operator=(Option &&) = default;

    /**
     * Construct a group title.
     * 
     * Parameters:
     *   doc - A title.
     */
    Option(const char *doc,
           const std::set<int> &flags = std::set<int>())
        : _doc(doc), _flags(flags) {}

    /**
     * Construct an option.
     * 
     * Parameters:
     *   names - A list of option names (e.g. {'q', "quiet"}).
     *   doc - A documentation string.
     *   flags - An optional set of flags describing the option.
     */
    Option(const std::vector<Name> &names,
           const char *doc = "",
           const std::set<int> &flags = std::set<int>())
        : _names(names), _doc(doc), _flags(flags) {}

    /**
     * Construct an option.
     * 
     * Parameters:
     *   names - A list of option names (e.g. {'q', "quiet"}).
     *   arg - An argument.
     *   doc - A documentation string.
     *   flags - An optional set of flags describing the option.
     */
    Option(const std::vector<Name> &names,
           const Argument &arg,
           const char *doc = "",
           const std::set<int> &flags = std::set<int>())
        : _names(names), _arg(arg), _doc(doc), _flags(flags) {}

    /**
     * Construct an option.
     * 
     * Parameters:
     *   names - A list of option names (e.g. {'q', "quiet"}).
     *   doc - A documentation string.
     *   arg - An argument.
     *   flags - An optional set of flags describing the option.
     */
    Option(const std::vector<Name> &names,
           const char *doc,
           const Argument &arg,
           const std::set<int> &flags = std::set<int>())
        : _names(names), _arg(arg), _doc(doc), _flags(flags) {}

    /**
     * Check whether the option has an argument.
     */
    bool has_argument() const
    {
        return _arg.has_value();
    }

    /**
     * Return the option names.
     */
    const std::vector<Name> &names() const
    {
        return _names;
    }

    /**
     * Return the argument.
     */
    const Argument &argument() const
    {
        return _arg.value();
    }

    /**
     * Return the documentation string.
     */
    const char *doc() const
    {
        return _doc;
    }

    /**
     * Return the flags.
     */
    const std::set<int> &flags() const
    {
        return _flags;
    }

private:
    std::vector<Name> _names;
    std::optional<Argument> _arg;
    const char *_doc = "";
    std::set<int> _flags;
};

} // namespace dopt

#endif