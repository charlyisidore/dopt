/* -*-c++-*-
    dopt - C++ command line option parser.

    This is free and unencumbered software released into the public domain.
*/

#ifndef DOPT_NAME_HPP
#define DOPT_NAME_HPP

#include <cstring>
#include <string>
#include <variant>

namespace dopt
{

/**
 * Represents a single option name.
 */
class Name
{
public:
    /**
     * Construct from a short name.
     * 
     * Parameters:
     *   key - A short name without the "-".
     */
    Name(char key)
        : _name(key)
    {
    }

    /**
     * Construct from a long name.
     * 
     * Parameters:
     *   name - A long name without the "--".
     */
    Name(const char *name)
        : _name(name)
    {
    }

    /**
     * Copy constructor.
     */
    Name(const Name &) = default;

    /**
     * Move constructor.
     */
    Name(Name &&) = default;

    /**
     * Copy assignment operator.
     */
    Name &operator=(const Name &) = default;

    /**
     * Move assignment operator.
     */
    Name &operator=(Name &&) = default;

    /**
     * Check whether the name is equal to a given short name without the "-".
     */
    bool operator==(char key) const
    {
        auto k = std::get_if<char>(&_name);
        return k && *k == key;
    }

    /**
     * Check whether the name is equal to a given long name without the "--".
     */
    bool operator==(const char *name) const
    {
        auto n = std::get_if<const char *>(&_name);
        return n && std::strcmp(*n, name) == 0;
    }

    /**
     * Check whether the name is short (e.g. "-n").
     */
    bool is_short() const
    {
        return std::holds_alternative<char>(_name);
    }

    /**
     * Check whether the name is long (e.g. "--name").
     */
    bool is_long() const
    {
        return std::holds_alternative<const char *>(_name);
    }

    /**
     * Cast as a short name (char) without the "-".
     */
    char get_short() const
    {
        return std::get<char>(_name);
    }

    /**
     * Cast as a long name (string) without the "--".
     */
    const char *get_long() const
    {
        return std::get<const char *>(_name);
    }

    /**
     * Return the name as a string (with "-" or "--").
     */
    std::string str() const
    {
        return is_short()
                   ? (std::string("-") + get_short())
                   : (std::string("--") + get_long());
    }

private:
    std::variant<char, const char *> _name;
};

/**
 * Defines an ordering on Name objects.
 */
inline bool operator<(const Name &n1, const Name &n2)
{
    if (n1.is_short() && n2.is_short())
    {
        // Compare two short options
        return n1.get_short() < n2.get_short();
    }
    else if (n1.is_long() && n2.is_long())
    {
        // Compare two long options
        return std::strcmp(n1.get_long(), n2.get_long()) < 0;
    }
    else
    {
        // Short options come first
        return n1.is_short();
    }
}

} // namespace dopt

#endif