

#include <dopt.hpp>
#include <iostream>
#include <string>

int main(int argc, char *argv[])
{
    using arg = dopt::Argument;

    dopt::ParserArgp parse{

        // A string describing the non-option arguments.
        "FILENAME",

        // A doc string about this program, displayed before the options.
        "This program does something interesting.",

        // The list of options.
        {
            // A title for a group of options.
            {"General options:"},
            // Some options:
            {{'n', "iterations"}, arg("N"), "Number of iterations"},
            {{"alpha"}, arg("FLOAT"), "Value of alpha"},
            {{'q', "quiet"}, "Do not display verbose information"}},

        // A doc string displayed after the options.
        "This part of the documentation comes after the options."};

    // Define option default values.
    std::string filename;
    int iterations = 100;
    double alpha = 0.5;
    bool verbose = true;

    // Parse the command line.
    // If --help is encountered, the program shows the documentation and exits.
    dopt::Map args = parse(argc, argv);

    // Check the number of positional arguments.
    if (args.size() == 0)
    {
        std::cout << "Please specify a filename" << std::endl;
        return 0;
    }

    // Get a positional argument.
    filename = args[0].str();

    // Check whether a named option has been specified and get its argument value.
    if (args['n']) // or result["iterations"]
    {
        iterations = args['n'].get<int>();
    }

    // Get a named argument with a default value.
    alpha = args["alpha"].get<double>(0.5);

    // Check whether a flag has been specified.
    verbose = !args['q'];

    // Print option values
    std::cout << "Program options:" << std::endl
              << "filename   = " << filename << std::endl
              << "iterations = " << iterations << std::endl
              << "alpha      = " << alpha << std::endl
              << "verbose    = " << verbose << std::endl;

    return 0;
}