# dopt

C++ command line option parser.

Author: Charly Lersteau

`dopt` is a lightweight and easy-to-use header-only C++17 library built on top of [`Argp`](https://www.gnu.org/software/libc/manual/html_node/Argp.html).
It helps defining command line options and automatically generates the `--help` documentation.


## Usage

Just include the `include/dopt.hpp` (standalone) file in your project.
Ensure that your compiler supports the `C++17` standard.

```cpp
#include <dopt.hpp>
```

In your `main(argc, argv)` function, describe the command line options.

```cpp
using arg = dopt::Argument;

dopt::ParserArgp parse{

    // A string describing the non-option arguments.
    "FILENAME",

    // A doc string about this program, displayed before the options.
    "This program does something interesting.",

    // The list of options.
    {
        // A title for a group of options.
        {"General options:"},
        // Some options:
        {{'n', "iterations"}, arg("N"), "Number of iterations"},
        {{"alpha"}, arg("FLOAT"), "Value of alpha"},
        {{'q', "quiet"}, "Do not display verbose information"}},

    // A doc string displayed after the options.
    "This part of the documentation comes after the options."};
```

Note that there is no need to describe the `--help` option, Argp will create it for you.
When `--help` is encountered, the program automatically shows the documentation and exits.

To mark the argument of `--alpha` as optional:

```cpp
{{"alpha"}, arg("FLOAT").optional(), "Value of alpha"},
```

Let us parse the command line arguments.

```cpp
dopt::Map args = parse(argc, argv);
```

Check the number of positional arguments.

```cpp
if (args.size() == 0)
{
    std::cout << "Please provide at least one positional argument" << std::endl;
    return 0;
}
```

Get a positional argument as a string.

```cpp
std::string filename = args[0].str();
```

Check whether a named option has been specified and get its argument value.

```cpp
int n = 1;
if (args['n']) // or result["iterations"]
{
    n = args['n'].get<int>();
}
```

Get a named argument with a default value.

```cpp
double alpha = args["alpha"].get<double>(0.5);
```

Check whether a flag has been specified.

```cpp
bool quiet = args['q'];
```

Running the example (`src/main.cpp`) with the `--help` option prints the following result:

```
Usage: dopt [OPTION...] FILENAME
This program does something interesting.

 General options:
      --alpha=FLOAT          Value of alpha
  -n, --iterations=N         Number of iterations
  -q, --quiet                Do not display verbose information

  -?, --help                 Give this help list
      --usage                Give a short usage message

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.

This part of the documentation comes after the options.
```


## Using getopt

`getopt` offers less features than `Argp` (such as generating the `--help` documentation) but is lighter and is available on more platforms.
`dopt` provides a wrapper for `getopt`.

```cpp
    using arg = dopt::Argument;

    dopt::ParserGetopt parse{
        {{{'n', "iterations"}, arg("N")},
         {{"alpha"}, arg("FLOAT")},
         {{'q', "quiet"}},
         {{'h', "help"}}}};
```

You can call the `parse` object the same way as with `dopt::ParserArgp`.


## Remarks

- The `include/dopt` folder contains the source code as separated files.
These files have been combined together into a single file `dopt.hpp` (by running `make header`). Therefore, you only need to include `dopt.hpp`.
- `dopt` actually has the following limitations that may be fixed in newer versions:
  - Although it is widely available through `glibc`, `dopt` depends on `Argp`.
  - `dopt` uses the default parsing function and documentation generator from `Argp`.
  - The string describing non-option arguments (in `Usage:`) is just descriptive. `dopt` does not check if the syntax is respected.
  - `dopt` does not check the type of arguments.


## License

This is free and unencumbered software released into the public domain.
For more information, please refer to <[https://unlicense.org](https://unlicense.org/)>


## Alternatives

Not satisfied?
Check out these other great CLI option parsers:

- [`argagg`](https://github.com/vietjtnguyen/argagg)
- [`argh`](https://github.com/adishavit/argh)
- [`Argp`](https://www.gnu.org/software/libc/manual/html_node/Argp.html)
- [`args`](https://github.com/taywee/args)
- [`Boost.Program_options`](https://www.boost.org/doc/libs/1_73_0/doc/html/program_options.html)
- [`CLI11`](https://cliutils.github.io/CLI11/book/)
- [`cxxopts`](https://github.com/jarro2783/cxxopts)
- [`docopt`](https://github.com/docopt/docopt.cpp)
- [`gengetopt`](https://www.gnu.org/software/gengetopt/gengetopt.html)
- [`getopt`](https://www.gnu.org/software/libc/manual/html_node/Getopt.html)
- [`gflags`](https://gflags.github.io/gflags/)
- [`Lyra`](https://github.com/bfgroup/Lyra)
- [`TCLAP`](http://tclap.sourceforge.net/)
- [`yaggo`](https://github.com/gmarcais/yaggo)
- [Daniel Petrovic's parser](https://www.codeproject.com/Tips/5261900/Cplusplus-lightweight-parsing-command-line-argumen)
- [Simon Schneegans' parser](https://schneegans.github.io/tutorials/2019/08/06/commandline)


## Resources

- (The Open Group) [Utility Argument Syntax](https://pubs.opengroup.org/onlinepubs/9699919799/basedefs/V1_chap12.html)
- (gnu.org) [Standards for Command Line Interfaces](https://www.gnu.org/prep/standards/html_node/Command_002dLine-Interfaces.html)
- (gnu.org) [Program Argument Syntax Conventions](https://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html)
- (sourceware.org) [Argp source](https://sourceware.org/git/?p=glibc.git;a=tree;f=argp;hb=HEAD)
- (sourceware.org) [getopt source](https://sourceware.org/git/?p=glibc.git;a=blob;f=posix/getopt.c;hb=HEAD)