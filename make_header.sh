#!/bin/bash
# Combine several .hpp headers into a standalone .hpp header

# Destination file
out="include/dopt.hpp"
dopt="dopt"

# Insert include guard
cat <<EOF > $out
/* -*-c++-*-
    dopt - C++ command line option parser.

    This is free and unencumbered software released into the public domain.
*/

#ifndef DOPT_HPP
#define DOPT_HPP

EOF

# Insert STL includes
sed '/#include <.*>/!d' include/${dopt}/*.hpp | sort | uniq >> $out

# Open namespace
cat <<EOF >> $out

namespace dopt
{
EOF

# Concatenate the sources; keep what is between "namespace dopt {" and "}".
# The code formatter makes it easier since it adds a comment after "}".
sed -s '/^namespace dopt/,/^} \/\/ namespace dopt/!d;/^namespace dopt/,+1d;/^} \/\/ namespace dopt/d' include/${dopt}/{argument,name,value,option,map,parser_argp,parser_getopt}.hpp >> $out

# Close namespace and include guard
cat <<EOF >> $out
}

#endif
EOF

# Just to show that the file has been written
echo "> include/dopt.hpp"